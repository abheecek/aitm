@extends('layouts.layout1')
@section('title', 'Edit Manufacture')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Manufacture</h1>
        <div class="table-responsive">
            <form method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Manufacture Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Manufacture Name" value="{{ $manufacture->name }}" />
                </div>
                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection