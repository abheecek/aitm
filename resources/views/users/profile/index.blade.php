@extends('layouts.layout1')
@section('title', 'User Profile')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <span>Profile</span>
            &nbsp;
            <a href="{{ url('profile/edit') }}" class="link">Edit</a>
        </h1>
    </div>
    <div class="col-lg-12">
        <div class="row mb10">
            <div class="col-lg-3">Full Name</div>
            <div class="col-lg-8">{{ $user->firstname }} {{ $user->lastname }}</div>
        </div>

        <div class="row mb10">
            <div class="col-lg-3">Username</div>
            <div class="col-lg-8">{{ $user->username }}</div>
        </div>

        <div class="row mb10">
            <div class="col-lg-3">Email</div>
            <div class="col-lg-8">{{ $user->email }}</div>
        </div>

        <div class="row mb10">
            <div class="col-lg-3">Job Title</div>
            <div class="col-lg-8">{{ $user->job_title }}</div>
        </div>

        <div class="row mb10">
            <div class="col-lg-3">Mobile</div>
            <div class="col-lg-8">{{ $user->mobile }}</div>
        </div>

        <div class="row mb10">
            <div class="col-lg-3">Department</div>
            <div class="col-lg-8">{{ $user->department_name }}</div>
        </div>

        <div class="row mb10">
            <div class="col-lg-3">Company</div>
            <div class="col-lg-8">{{ $user->company_name }}</div>
        </div>

        <div class="row mb10">
            <div class="col-lg-3">Location</div>
            <div class="col-lg-8">{{ $user->location_name }}</div>
        </div>

        <div class="row mb10">
            <div class="col-lg-3">Profile Image</div>
            <div class="col-lg-8">
                <img src="{{ asset("imgs/profile-75.jpg") }}" />
            </div>
        </div>
    </div>
</div>
@endsection