@extends('layouts.layout1')
@section('title', 'Edit User')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit User</h1>
        <div class="table-responsive">
            <form method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">First Name</label>
                    <input type="text" name="firstname" class="form-control" placeholder="First Name" value="{{ $user->firstname }}" />
                </div>

                <div class="form-group">
                    <label for="name">Last Name</label>
                    <input type="text" name="lastname" class="form-control" placeholder="Last Name" value="{{ $user->lastname }}"  />
                </div>

                <div class="form-group">
                    <label for="name">Email</label>
                    <input type="text" name="email" class="form-control" placeholder="Email" value="{{ $user->email }}"  />
                </div>

                <div class="form-group">
                    <label for="name">Username</label>
                    <input type="text" name="username" class="form-control" placeholder="Username" value="{{ $user->username }}"  />
                </div>

                <div class="form-group">
                    <label for="name">Password</label>
                    <input type="password" name="password" class="form-control" placeholder="Password" />
                </div>

                <div class="form-group">
                    <label for="name">User Role</label>
                    <select name="usertype" class="form-control">
                        @foreach($User->user_types as $user_type)
                        <option value="{{ $user_type }}" @if($user->usertype == $user_type) selected @endif >{{ $user_type }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Job Title</label>
                    <input type="text" name="job_title" class="form-control" placeholder="Job Title" value="{{ $user->job_title }}"  />
                </div>

                <div class="form-group">
                    <label for="name">Mobile</label>
                    <input type="text" name="mobile" class="form-control" placeholder="Mobile" value="{{ $user->mobile }}"  />
                </div>

                <div class="form-group">
                    <label for="name">Emp. Code</label>
                    <input type="text" name="emp_code" class="form-control" placeholder="Emp. Code" value="{{ $user->emp_code }}"  />
                </div>


                <div class="form-group">
                    <label for="name">Company</label>
                    <select name="company_id" class="form-control">
                        @foreach($Company->getCompanies() as $com)
                        <option value="{{ $com->id }}" @if($user->company_id == $com->id) selected @endif>{{ $com->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Location</label>
                    <select name="location_id" class="form-control">
                        @foreach($Location->getLocations() as $loc)
                        <option value="{{ $loc->id }}" @if($user->location_id == $loc->id) selected @endif>{{ $loc->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Department</label>
                    <select name="department_id" class="form-control">
                        @foreach($Department->getDepartments() as $dep)
                        <option value="{{ $dep->id }}" @if($user->department_id == $dep->id) selected @endif>{{ $dep->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Notes</label>
                    <textarea name="notes" class="form-control">{{ $user->notes }}</textarea>
                </div>

                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection