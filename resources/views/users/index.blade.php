@extends('layouts.layout1')
@section('title', 'Users')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <span>Users</span>
            &nbsp;
            <a href="{{ url('user/add') }}" class="link">Add</a>
        </h1>
        <div class="table-responsive">
            <table class="mytable table table-striped">
                <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Full Name</th>
                        <th>Username</th>
                        <th>User Role</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Department</th>
                        <th>Location</th>
                        <th>Company</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($User->getUsers() as $user)
                    <tr>
                        <td>
                            <a href="{{ url('user/edit') }}/{{ $user->id }}" data-id="{{ $user->id }}" >Edit</a>
                        </td>
                        <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->usertype }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->mobile }}</td>
                        <td>{{ $user->department_name }}</td>
                        <td>{{ $user->location_name }}</td>
                        <td>{{ $user->company_name }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection