@extends('layouts.layout1')
@section('title', 'Create Ticket')

@push('lib')
    <link href="{!! CommonHelper::asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') !!}" rel="stylesheet" />
@endpush

@section('content')
    <section class="content-header">
        <h1 class="title-capitalize">Create Ticket</h1>

        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">Ticket</li>
        </ol>
    </section>
    <section class="content">
        {{-- BOLLATI --}}
        <div class="row" style="margin-top: 20px">
            <div class="col-md-12">
                <form method="POST" action="" role="form" id="createTicket" class="form-horizontal">
	                <div class="form-group">
						<label for="ticket_title" class="col-md-3 control-label">Request Title : </label>
					    <div class="input-group col-md-4">
					        <input placeholder="Title" class="form-control ticket_title" name="ticket_title" id="ticket_title" type="text" required>
					    </div>
					</div>
	                <div class="form-group">
						<label for="ticket_type" class="col-md-3 control-label">Request Type : </label>
					    <div class="input-group col-md-4">
					    	<select class="ticket_type selectpicker col-md-6" data-style="btn-primary" id="ticket_type" name="ticket_type" required>
					    		<option value="Priority 1">Priority 1</option>
					    		<option value="Priority 2">Priority 2</option>
					    		<option value="Priority 3">Priority 3</option>
					    		<option value="Priority 4">Priority 4</option>
					    	</select>
					    </div>
					</div>
					
					<div class="form-group">
						<label for="analytic_end" class="col-md-3 control-label">Description : </label>
					    <div class="input-group col-md-4">
					    	<textarea rows="" cols="" name="description" class="description form-control" required></textarea>
					    </div>
					</div>
					
					<div class="form-group">
            			<label for="expected_date" class="control-label col-md-3">Expected Date : </label>
            			<div data-date-start-date="+0d" data-date-format="dd-mm-yyyy" class="input-group date expected_date col-md-4">	
							<span class="input-group-addon showCalender">
                				<i class="fa fa-calendar"></i>
            				</span>	
			                <input id="expected_date" name="expected_date" data-fieldtype="timestamp" data-typecast="date" readonly="" class="datepicker form-control expected_date" value="" type="text" required>
						</div>
					</div>
					
					<div class="form-group">
						<div class="form-actions">
						 	<div class="col-md-3"></div>
							<div class="col-md-4">
								<button class="btn blue" type="submit">Submit</button>
							</div>
						</div>
					</div>
					{{ csrf_field() }}
	                </form>
            </div>
        </div>
    </section>
@stop

@push('lib')
    <script src="{!! CommonHelper::asset('js/ticket.js') !!}"></script>
    <script src="{!! CommonHelper::asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}"></script>
@endpush