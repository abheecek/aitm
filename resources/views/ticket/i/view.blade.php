@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    View Ticket
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1 class="title-capitalize">View Ticket</h1>

        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">Ticket</li>
        </ol>
    </section>
    <section class="content">
        {{-- BOLLATI --}}
        <div class="row" style="margin-top: 20px">
            <div class="col-md-12">
                @beginbox(['title' => 'View', 'titleClass' => 'panel-primary', 'id' => 'analyticMarginBox','iconTitle' => 'fa-files-o', 'enableCollapse' => false, 'enableClose' => false])
                <form method="POST" action="{{ route('admin.iticket.fix', ['id' => $id]) }}" role="form" id="resolveTicket" class="form-horizontal">
	                <div class="form-group">
						<label for="ticket_title" class="col-md-3 control-label">Request Title : </label>
					    <div class="col-md-4" style="padding-top:7px; ">
					        {{$ticket->ticket_title}}
					    </div>
					</div>
					
	                <div class="form-group">
						<label for="ticket_type" class="col-md-3 control-label">Request Type : </label>
					    <div class="col-md-4" style="padding-top:7px; ">
					        {{$ticket->ticket_type}}
					    </div>
					</div>
					
					<div class="form-group">
						<label for="analytic_end" class="col-md-3 control-label">Problem Description : </label>
					   <div class="col-md-4" style="padding-top:7px; ">
					        {{$ticket->prob_desc}}
					    </div>
					</div>
					
					<div class="form-group">
            			<label for="expected_date" class="control-label col-md-3">Expected Date : </label>
            			<div class="col-md-4" style="padding-top:7px; ">
					        {{date('d-m-Y', strtotime($ticket->expected_date))}}
					    </div>
					</div>
					
					<div class="form-group">
            			<label for="expected_date" class="control-label col-md-3">Created Date : </label>
            			<div class="col-md-4" style="padding-top:7px; ">
					        {{date('d-m-Y', strtotime($ticket->created_at))}}
					    </div>
					</div>
					
					<div class="form-group">
						<label for="ticket_type" class="col-md-3 control-label">Status : </label>
					    <div class="input-group col-md-4">
					    	<select class="ticket_type form-control" id="status" name="status" required>
					    		<option value="">--Status--</option>
					    		@if(count($ticket_status) > 0)
					    			@foreach($ticket_status as $k => $v)
					    				@if($ticket->status == $k)
					    					<option value="{{$k}}" selected="selected">{{$v}}</option>
					    				@else
					    					<option value="{{$k}}">{{$v}}</option>
					    				@endif
					    			@endforeach
					    		@endif
					    	</select>
					    </div>
					</div>
					
					<div class="form-group">
						<label for="analytic_end" class="col-md-3 control-label">Comments : </label>
						<div class="input-group col-md-4">
							@if(count($ticket_desc) > 0)
								@foreach($ticket_desc as $k => $v)
									@if($v->byit == 1)
										<div class="text-right it-wrap">
											<b>Me: </b>{{$v->desc}}
										</div>
									@else
										<div class="text-left cust-wrap">
											<b>Customer: </b>{{$v->desc}}
										</div>
									@endif
								@endforeach
							@endif
							@if($ticket->status != 2)
							<div class="new-wrap">
								<textarea placeholder="New Comment" rows="" cols="" name="description" class="description form-control" required>{{$ticket->sol_desc}}</textarea>
							</div>
							@endif
					    </div>
					</div>
					
					<div class="form-group">
						<div class="form-actions">
						 	<div class="col-md-2"></div>
							<div class="col-md-1">
								<a class="btn btn-primary" href="{{ route('admin.iticket.list') }}">Back</a>
							</div>
							
							<div class="col-md-1">
								<input type="hidden" name="id" value="{{$id}}">
								{{ csrf_field() }}
								<button type="submit" value="Close" class="btn btn-success close-ticket">Submit</button>
							</div>
						</div>
					</div>
	            </form>
                @closebox
            </div>
        </div>
    </section>
@stop

@section('footer_scripts')
    {{-- Data Tables --}}
    {!! Html::script('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js') !!}
    {!! Html::script('assets/js/pages/ticket.js') !!}
@stop

