@extends('admin.layouts.default')
{{-- Page title --}}
@section('title')
    Tickets
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css"/>
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Tickets</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    {{ trans('general.dashboard') }}
                </a>
            </li>
            <li class="active">Tickets</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @beginbox(['title' => 'Tickets', 'titleClass' => 'panel-primary', 'id' => 'ticketList', 'iconTitle' => 'fa-user', 'enableCollapse' => false, 'enableClose' => false])

                <table class="table table-bordered " id="ticketDataTable">
                    <thead>
                    <tr class="filters">
                        <th>Title</th>
                        <th>Type</th>
                        <th>Expected Time</th>
                        <th>Created Time</th>
                        <th>Status</th>
                        <th>View</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

                @closebox
            </div>
        </div>    <!-- row-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>
    {!! Html::script('assets/vendors/datetime-moment/js/datetime-moment.js') !!}
   
    <script>
        var ticketDataTable;

        $(document).ready(function () {
        	$('#ticketDataTable').DataTable( {
                "ajax": '{!! route("admin.iticket.filterData") !!}'
            });
        });

        $(function () {
            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        });
    </script>
@stop