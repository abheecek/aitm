@extends('layouts.layout1')
@section('title', 'Add Model')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Model</h1>
        <div class="table-responsive">
            <form method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Manufacture</label>
                    <select name="manufact_id" class="form-control">
                        @foreach($objManufacture->getManufactures() as $m)
                        <option value="{{ $m->id }}">{{ $m->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Model Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Model Name">
                </div>
                <div class="form-group">
                    <label for="name">Category</label>
                    <select name="category_id" class="form-control">
                        @foreach($objCategory->getCategories() as $ct)
                        <option value="{{ $ct->id }}">{{ $ct->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Model No.</label>
                    <input type="text" name="model_no" class="form-control" placeholder="Model No.">
                </div>
                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection