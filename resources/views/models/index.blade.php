@extends('layouts.layout1')
@section('title', 'Models')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <span>Models</span>
            &nbsp;
            <a href="{{ url('model/add') }}" class="link">Add</a>
        </h1>
        <div class="table-responsive">
            <table class="mytable table table-striped">
                <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Manufacture</th>
                        <th>Model Name</th>
                        <th>Model No.</th>
                        <th>Category</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objModel->getModels() as $mdl)
                    <tr>
                        <td>
                            <a href="{{ url('model/edit') }}/{{ $mdl->id }}" data-id="{{ $mdl->id }}" >Edit</a>
                        </td>
                        <td>{{ $mdl->manufact_name }}</td>
                        <td>{{ $mdl->name }}</td>
                        <td>{{ $mdl->model_no }}</td>
                        <td>{{ $mdl->cat_name }}</td>
                        <td>{{ $mdl->category_type }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection