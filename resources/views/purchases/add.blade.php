@extends('layouts.layout1')
@section('title', 'Add Purchase')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Purchase</h1>
        <form method="post" class="form-horizontal">
            {{ csrf_field() }}

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="purchase_date" class="control-label">Purchase Date</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="purchase_date" id="purchase_date" class="form-control" placeholder="Enter Purchase Date">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="invoice_number" class="control-label">Invoice Number</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="invoice_number" id="invoice_number" class="form-control" placeholder="Enter Invoice Number">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="company_id" class="control-label">Purchase For Company</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <select name="company_id" id="company_id" class="form-control">
                        @foreach($objCompany->getCompanies(array("id", "name"), "name") as $comp)
                        <option value="{{ $comp->id }}">{{ $comp->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="supplier_id" class="control-label">Supplier</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <select name="supplier_id" id="supplier_id" class="form-control">
                        @foreach($objSupplier->getSuppliersForOpts() as $supplier)
                        <option value="{{ $supplier->id }}">{{ $supplier->supplier_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="notes" class="control-label">Notes</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <textarea name="notes" id="notes" class="form-control minTextArea"></textarea>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    &nbsp;
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <button type="submit" id="form_submit" class="btn btn-default">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('head')
@include('includes.csslib')
@endpush

@push('lib')
@include('includes.jslib')
<script type="text/javascript" src="{!! CommonHelper::asset('js/purchase/add.js') !!}"></script>
@endpush