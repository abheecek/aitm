@extends('layouts.layout1')
@section('title', 'Edit Purchase')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Purchase</h1>
        <form method="post">
            {{ csrf_field() }}

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="purchase_date" class="control-label">Purchase Date</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="purchase_date" id="purchase_date" class="form-control" placeholder="Enter Purchase Date" value="{{ $purchase->purchase_date }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="invoice_number" class="control-label">Invoice Number</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="invoice_number" id="invoice_number" class="form-control" placeholder="Enter Invoice Number" value="{{ $purchase->invoice_number }}">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="company_id" class="control-label">Purchase For Company</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <select name="company_id" id="company_id" class="form-control">
                        @foreach($objCompany->getCompanies(array("id", "name"), "name") as $comp)
                        <option value="{{ $comp->id }}" @if($purchase->company_id == $comp->id) selected  @endif>{{ $comp->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="supplier_id" class="control-label">Supplier</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <select name="supplier_id" id="supplier_id" class="form-control">
                        @foreach($objSupplier->getSuppliersForOpts() as $supplier)
                        <option value="{{ $supplier->id }}" @if($purchase->supplier_id == $supplier->id) selected  @endif>{{ $supplier->supplier_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="notes" class="control-label">Notes</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <textarea name="notes" id="notes" class="form-control minTextArea">{{ $purchase->notes }}</textarea>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    &nbsp;
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <button type="submit" id="form_submit" class="btn btn-default">Save</button>
                </div>
            </div>

        </form>
    </div>
</div>
@endsection