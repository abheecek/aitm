@extends('layouts.layout1')
@section('title', 'Edit Department')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Department</h1>
        <div class="table-responsive">
            <form method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Department Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Department Name" value="{{ $department->name }}" />
                </div>

                <div class="form-group">
                    <label for="name">Location</label>
                    <select name="location_id" class="form-control">
                        @foreach($objLocation->getLocations() as $loc)
                        <option value="{{ $loc->id }}" @if($department->location_id == $loc->id) selected @endif >{{ $loc->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection