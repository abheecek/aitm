@extends('layouts.layout1')
@section('title', 'Departments')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <span>Departments</span>
            &nbsp;
            <a href="{{ url('department/add') }}" class="link">Add</a>
        </h1>
        <div class="table-responsive">
            <table class="mytable table table-striped">
                <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Department Name</th>
                        <th>Location Name</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objDepartment->getDepartments() as $dpmt)
                    <tr>
                        <td>
                            <a href="{{ url('department/edit') }}/{{ $dpmt->id }}" data-id="{{ $dpmt->id }}" >Edit</a>
                        </td>
                        <td>{{ $dpmt->name }}</td>
                        <td>{{ $dpmt->location_name }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection