@extends('layouts.layout1')
@section('title', 'Suppliers')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <span>Suppliers</span>
            &nbsp;
            <a href="{{ url('supplier/add') }}" class="link">Add</a>
        </h1>
        <div class="table-responsive">
            <table class="mytable table table-striped">
                <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Supplier Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Zip</th>
                        <th>Country</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objSupplier->getSuppliers() as $supplier)
                    <tr>
                        <td>
                            <a href="{{ url('supplier/edit') }}/{{ $supplier->id }}" data-id="{{ $supplier->id }}" >Edit</a>
                        </td>
                        <td>{{ $supplier->name }}</td>
                        <td>{{ $supplier->email }}</td>
                        <td>{{ $supplier->phone }}</td>
                        <td>{{ $supplier->city }}</td>
                        <td>{{ $supplier->state }}</td>
                        <td>{{ $supplier->zip }}</td>
                        <td>{{ $supplier->country_name }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection