@extends('layouts.layout1')
@section('title', 'Add Supplier')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Supplier</h1>
        <form method="post" class="form-horizontal">
            {{ csrf_field() }}

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="name" class="control-label">Supplier Name</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="name" id="name" class="form-control" placeholder="Enter Supplier Name">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="contact" class="control-label">Contact Person Name</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="contact" id="contact" class="form-control" placeholder="Enter Contact Person Name">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="address" class="control-label">Street 1</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="address" id="address" class="form-control" placeholder="Enter Street 1">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="address2" class="control-label">Street 2</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="address2" id="address2" class="form-control" placeholder="Enter Street 2">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="city" class="control-label">City</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="city" id="city" class="form-control" placeholder="Enter City">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="state" class="control-label">State</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="state" id="state" class="form-control" placeholder="Enter State">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="country" class="control-label">Country</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <select name="country" id="country" class="form-control">
                        @foreach($objSupplier->getCountries() as $country)
                        <option value="{{ $country->country_code }}">{{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="zip" class="control-label">Zip/Postal Code</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="zip" id="zip" class="form-control" placeholder="Zip/Postal Code">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="phone" class="control-label">Phone</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Enter Phone">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="email" class="control-label">Email Address</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email Address">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="url" class="control-label">Website URL</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <input type="text" name="url" id="url" class="form-control" placeholder="Enter Website URL">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    <label for="notes" class="control-label">Notes</label>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-8">
                    <textarea name="notes" id="notes" class="form-control minTextArea"></textarea>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-sm-12 col-md-4 col-lg-4 text-right">
                    &nbsp;
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <button type="submit" id="form_submit" class="btn btn-default">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection