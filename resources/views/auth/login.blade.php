@extends('layouts.layout0')
@section('title', 'Login')

@section('content')
<div class="layout-app col-fs">
    <div class="row row-app">
        <div class="col-lg-12">
        	<div class="col-separator col-separator-first box col-unscrollable col-fs">
        		<div class="col-table">
					<div class="col-table-row">
						<div class="col-app col-unscrollable tab-content">
							<div class="col-app lock-wrapper lock-bg-1 tab-pane active animated fadeIn" id="lock-1-1">
					            <h3 class="text-white innerB text-center">Login</h1>
					            <div class="lock-container">
					            	<div class="innerAll text-center">
						                <form method="post">
						                	<img src="{!! CommonHelper::asset('/theme/assets/images/people/100/22.jpg') !!}" class="img-circle"/>
											<div class="innerLR">
						                        <input type="text" name="username" class="form-control text-center bg-gray" placeholder="Enter Username">
						                        <input type="password" name="password" class="form-control text-center bg-gray" placeholder="Enter Password">
						                    </div>
						                    <div class="innerT">
						                        {{ csrf_field() }}
							                    <button type="submit" class="btn btn-primary">Login</button>
						                    </div>
						                    <a href="#" class="btn margin-none">Forgot password?</a>
						                </form>
									</div>						                
					            </div>
							</div>					           
						</div>
					</div>
				</div>					            
            </div>
        </div>
    </div>
</div>
@endsection