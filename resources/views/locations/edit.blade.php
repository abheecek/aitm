@extends('layouts.layout1')
@section('title', 'Edit Location')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Location</h1>
        <div class="table-responsive">
            <form method="post">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">Location Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Location Name"  value="{{ $location->name }}" >
                </div>

                <div class="form-group">
                    <label for="name">Street 1</label>
                    <input type="text" name="street1" class="form-control" placeholder="Street 1"  value="{{ $location->street1 }}" >
                </div>

                <div class="form-group">
                    <label for="name">Street 2</label>
                    <input type="text" name="street2" class="form-control" placeholder="Street 2"  value="{{ $location->street2 }}" >
                </div>

                <div class="form-group">
                    <label for="name">City</label>
                    <input type="text" name="city" class="form-control" placeholder="City"  value="{{ $location->city }}" >
                </div>

                <div class="form-group">
                    <label for="name">State</label>
                    <input type="text" name="state" class="form-control" placeholder="State"  value="{{ $location->state }}" >
                </div>

                <div class="form-group">
                    <label for="name">Country</label>
                    <select name="country" class="form-control" >
                        @foreach($objLocation->getCountries() as $country)
                        <option value="{{ $country->country_code }}" @if($location->country == $country->country_code) selected @endif >{{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Zip/Postal Code</label>
                    <input type="text" name="zip" class="form-control" placeholder="Zip/Postal Code"  value="{{ $location->zip }}" >
                </div>

                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection