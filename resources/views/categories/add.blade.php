@extends('layouts.layout1')
@section('title', 'Add Category')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Category</h1>
        <div class="table-responsive">
            <form method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Category Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Category Name">
                </div>
                <div class="form-group">
                    <label for="name">Category Type</label>
                    <select name="category_type" class="form-control">
                        <option value="Device">Device</option>
                        <option value="Asseccory">Asseccory</option>
                        <option value="Component">Component</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Save</button>
            </form>
        </div>
    </div>
</div>
@endsection