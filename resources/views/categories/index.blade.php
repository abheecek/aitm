@extends('layouts.layout1')
@section('title', 'Categories')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <span>Categories</span>
            &nbsp;
            <a href="{{ url('category/add') }}" class="link">Add</a>
        </h1>
        <div class="table-responsive">
            <table class="mytable table table-striped">
                <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Category Name</th>
                        <th>Category Type</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($category->getCategories() as $cat)
                    <tr>
                        <td>
                            <a href="{{ url('category/edit') }}/{{ $cat->id }}" data-id="{{ $cat->id }}" >Edit</a>
                        </td>
                        <td>{{ $cat->name }}</td>
                        <td>{{ $cat->category_type }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection