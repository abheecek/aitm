@extends('layouts.layout1')
@section('title', 'Status Labels')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <span>Status Labels</span>
            &nbsp;
            <a href="{{ url('status-label/add') }}" class="link">Add</a>
        </h1>
        <div class="table-responsive">
            <table class="mytable table table-striped">
                <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Status Label</th>
                        <th>Status Type</th>
                        <th>Notes</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objLabel->getLabels() as $lbl)
                    <tr>
                        <td>
                            <a href="{{ url('status-label/edit') }}/{{ $lbl->id }}" data-id="{{ $lbl->id }}" >Edit</a>
                        </td>
                        <td>{{ $lbl->name }}</td>
                        <td>{{ $lbl->status_type }}</td>
                        <td>{{ $lbl->notes }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection