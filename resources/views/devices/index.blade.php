@extends('layouts.layout1')
@section('title', 'Devices')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <span>Devices</span>
            &nbsp;
            <a href="{{ url('device/add') }}" class="link">Add</a>
        </h1>
        <div class="table-responsive">
            <table class="mytable table table-striped">
                <thead>
                    <tr>
                        <th>Actions</th>
                        <th>Device Tag</th>
                        <th>Device Model</th>
                        <th>Manufacture</th>
                        <th>Company</th>
                        <th>Location</th>
                        <th>Status</th>
                        <th>User</th>
                        <th>Department</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($Device->getDevices() as $dev)
                    <tr>
                        <td>
                            <a href="{{ url('device/edit') }}/{{ $dev->id }}" data-id="{{ $dev->id }}" >Edit</a>
                            @if( $dev->is_deployed )
                                <a href="{{ url('device/checkin') }}/{{ $dev->id }}" data-id="{{ $dev->id }}" >Check In</a>
                            @endif
                            @if( $dev->is_deployable )
                                <a href="{{ url('device/checkout') }}/{{ $dev->id }}" data-id="{{ $dev->id }}" >Check Out</a>
                            @endif
                        </td>
                        <td>{{ $dev->device_tag }}</td>
                        <td>{{ $dev->model }}</td>
                        <td>{{ $dev->manufacture }}</td>
                        <td>{{ $dev->company }}</td>
                        <td>{{ $dev->location }}</td>
                        <td>{{ $dev->label }}</td>
                        <td>{{ $dev->user }}</td>
                        <td>{{ $dev->department }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection