<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7 paceCounter paceSocial app"> <![endif]-->
<!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8 paceCounter paceSocial app"> <![endif]-->
<!--[if IE 8]>    <html class="ie lt-ie9 paceCounter paceSocial app"> <![endif]-->
<!--[if gt IE 8]> <html class="ie paceCounter paceSocial app"> <![endif]-->
<!--[if !IE]><!--><html class="paceCounter paceSocia app"><!-- <![endif]-->

<head>
	<link rel="icon" href="favicon.ico">
	<title>@yield('title') - ITM</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    
	<link href="{!! CommonHelper::asset('/theme/assets/css/admin/module.admin.stylesheet-complete.min.css') !!}" rel="stylesheet" />
	<!-- <link href="https://fonts.googleapis.com/css?family=PT+Serif|Port+Lligat+Slab|Slabo+13px|Slabo+27px" rel="stylesheet" />-->
	<script>
        if( /*@cc_on!@*/ false && document.documentMode === 10){
            document.documentElement.className += ' ie ie10';
        }
    </script>	
	@stack('head')
</head>
<body>
	<div class="container-fluid menu-hidden">
		<div id="content">
			@if(Session::has('msg'))
			<div class="masalert alert alert-{{ Session::get('msg')['status'] }}">
				<a class="close" data-dismiss="alert">X</a> {!!
				Session::get('msg')['msg'] !!}
			</div>
			{{!! Session::forget('msg') !!} 
			@endif 
			@yield('content')
		</div>
		<div id="footer" class="hidden-print">
		    <div class="copy"> Proudly developed by - Nextmegabyte. 2016 - {{date('Y')}}</div>
		</div>
	</div>
	<script data-id="App.Config">
	    var App = {};
	    var basePath = '',
	        commonPath = '../../../public/assets/',
	        rootPath = '../',
	        DEV = false,
	        componentsPath = '../assets/components/';
	    var primaryColor = '#25ad9f',
	        dangerColor = '#b55151',
	        successColor = '#609450',
	        infoColor = '#4a8bc2',
	        warningColor = '#ab7a4b',
	        inverseColor = '#45484d';
	    var themerPrimaryColor = primaryColor;
	</script>
	<script src="{!! CommonHelper::asset('js/jquery-2.1.1.js') !!}"></script>
	<script src="{!! CommonHelper::asset('js/bootstrap.min.js') !!}"></script>
	<script src="{!! CommonHelper::asset('plugins/scrollTo/jquery.scrollTo.min.js') !!}"></script>
</body>
</html>