<!DOCTYPE html>

<html class="sidebar sidebar-social">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="favicon.ico">
        <title>@yield('title') - ITM</title>
        <script>
	        if( /*@cc_on!@*/ false && document.documentMode === 10){
	            document.documentElement.className += ' ie ie10';
	        }
	    </script>
        @stack('head')
        <link href="{!! CommonHelper::asset('/theme/assets/css/admin/module.admin.stylesheet-complete.min.css') !!}" rel="stylesheet" />
    </head>
    <body>
        <div class="container-fluid menu-hidden">
        	@include("includes.nav")
			<div id="content">
				@include("includes.header")
                @if(Session::has('msg'))
                <div class="masalert alert alert-{{ Session::get('msg')['status'] }}">
                    <a class="close" data-dismiss="alert">X</a>
                    {!! Session::get('msg')['msg'] !!}
                </div>
                {!! Session::forget('msg') !!}
                @endif 
                @yield('content')
	        </div>
        </div>

        <script src="{!! CommonHelper::asset('js/jquery-2.1.1.js') !!}"></script>
        <script src="{!! CommonHelper::asset('js/bootstrap.min.js') !!}"></script>
        <script src="{!! CommonHelper::asset('plugins/scrollTo/jquery.scrollTo.min.js') !!}"></script>
        <script src="{!! CommonHelper::asset('js/common.js') !!}"></script>

		<script data-id="App.Config">
		var App = {};	var basePath = '',
			commonPath = '../assets/',
			rootPath = '../',
			DEV = false,
			componentsPath = '../assets/components/';
		
		var primaryColor = '#25ad9f',
			dangerColor = '#b55151',
			successColor = '#609450',
			infoColor = '#4a8bc2',
			warningColor = '#ab7a4b',
			inverseColor = '#45484d';
		
		var themerPrimaryColor = primaryColor;
		</script>

		<script src="{!! CommonHelper::asset('theme/assets/library/jquery/jquery.min.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/library/jquery/jquery-migrate.min.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/library/modernizr/modernizr.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/core_less-js/less.min.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/charts_flot/excanvas.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/core_browser/ie/ie.prototype.polyfill.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script>if (/*@cc_on!@*/false && document.documentMode === 10) { document.documentElement.className+=' ie ie10'; }</script>

		<script src="{!! CommonHelper::asset('theme/assets/plugins/core_nicescroll/jquery.nicescroll.min.js') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/core_breakpoints/breakpoints.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/core_preload/pace.min.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/core_preload/preload.pace.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/menu_sidr/jquery.sidr.js?v=v2.0.0-rc8') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/forms_elements_bootstrap-switch/js/bootstrap-switch.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_elements_bootstrap-switch/bootstrap-switch.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_elements_fuelux-checkbox/fuelux-checkbox.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/ui_modals/bootbox.min.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/ui_modals/modals.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/notifications_gritter/js/jquery.gritter.min.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/admin_notifications_gritter/gritter.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/forms_editors_wysihtml5/js/wysihtml5-0.3.0_rc2.min.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/forms_editors_wysihtml5/js/bootstrap-wysihtml5-0.0.2.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_editors_wysihtml5/wysihtml5.init.js?v=v2.0.0-rc8') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/forms_wizards/jquery.bootstrap.wizard.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_wizards/form-wizards.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_elements_fuelux-radio/fuelux-radio.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/forms_elements_jasny-fileupload/js/bootstrap-fileupload.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_elements_button-states/button-loading.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/forms_elements_bootstrap-select/js/bootstrap-select.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_elements_bootstrap-select/bootstrap-select.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/forms_elements_select2/js/select2.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_elements_select2/select2.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/forms_elements_multiselect/js/jquery.multi-select.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_elements_multiselect/multiselect.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/forms_elements_inputmask/jquery.inputmask.bundle.min.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_elements_inputmask/inputmask.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/forms_elements_bootstrap-datepicker/js/bootstrap-datepicker.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_elements_bootstrap-datepicker/bootstrap-datepicker.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/forms_elements_bootstrap-timepicker/js/bootstrap-timepicker.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_elements_bootstrap-timepicker/bootstrap-timepicker.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/forms_elements_colorpicker-farbtastic/js/farbtastic.min.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/forms_elements_colorpicker-farbtastic/colorpicker-farbtastic.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/menus/sidebar.main.init.js?v=v2.0.0-rc8') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/menus/sidebar.collapse.init.js?v=v2.0.0-rc8') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/menus/menus.sidebar.chat.init.js?v=v2.0.0-rc8') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/other_mixitup/jquery.mixitup.min.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/plugins/other_mixitup/mixitup.init.js?v=v2.0.0-rc8&sv=v0.0.1.2') !!}"></script>
		<script src="{!! CommonHelper::asset('theme/assets/components/core/core.init.js?v=v2.0.0-rc8') !!}"></script>	
		
        
        
        @stack('lib')
    </body>
</html>