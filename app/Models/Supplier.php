<?php

namespace App\Models;

use App\Models\Base;
use DB;

class Supplier extends Base {

    public function getSuppliers() {
        return DB::table($this->tblSupplier . " as s")
                        ->leftJoin($this->tblCountry . " as c", "c.country_code", "=", "s.country")
                        ->addSelect("s.*")
                        ->addSelect("c.name as country_name")
                        ->get();
    }

    public function getSupplierById($id) {
        return DB::table($this->tblSupplier)->where("id", "=", $id)->limit(1)->get();
    }

    public function addSupplier($data) {
        $data["created_at"] = $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblSupplier)->insert($data);
    }

    public function updateSupplierById($data, $id) {
        $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblSupplier)->where("id", $id)->update($data);
    }

    public function getSuppliersForOpts() {
        return DB::table($this->tblSupplier)
                        ->select(DB::raw('concat(name, " (", city, ")") as supplier_name'), "id")
                        ->orderBy('supplier_name', 'asc')
                        ->get();
    }

}
