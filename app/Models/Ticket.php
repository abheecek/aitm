<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model{
   
    protected $table = 'ticket';
    public $timestamps = false;
    
    public function plan_desc(){
    	return $this->hasMany('AsteBolaffi\Models\TicketDesc');
    }
}
