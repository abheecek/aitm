<?php

namespace App\Models;

use App\Models\Base;
use DB;

class Category extends Base {

    public $category_types = array("Device", "Accessory", "Component");

    public function getCategories() {
        return DB::table($this->tblCategory)->get();
    }

    public function getCategoryById($id) {
        return DB::table($this->tblCategory)->where("id", "=", $id)->limit(1)->get();
    }

    public function addCategory($data) {
        $data["created_at"] = $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblCategory)->insert($data);
    }

    public function updateCategoryById($data, $id) {
        $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblCategory)->where("id", $id)->update($data);
    }

}
