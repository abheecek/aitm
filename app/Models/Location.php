<?php

namespace App\Models;

use App\Models\Base;
use DB;

class Location extends Base {

    public function getLocations() {
        return DB::table($this->tblLocation . " as l")
                        ->join($this->tblCompany . " as comp", "comp.id", "=", "l.company_id")
                        ->leftJoin($this->tblCountry . " as c", "c.country_code", "=", "l.country")
                        ->addSelect("l.*")
                        ->addSelect("c.name as country_name")
                        ->get();
    }

    public function getLocationById($id) {
        return DB::table($this->tblLocation)->where("id", "=", $id)->limit(1)->get();
    }

    public function addLocation($data) {
        $data["created_at"] = $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblLocation)->insert($data);
    }

    public function updateLocationById($data, $id) {
        $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblLocation)->where("id", $id)->update($data);
    }

    public function getLocationsForOpts() {
        return DB::table($this->tblLocation)
                        ->select(DB::raw('concat(name, " (", city, ")") as location_name'), "id")
                        ->orderBy('location_name', 'asc')
                        ->get();
    }

}
