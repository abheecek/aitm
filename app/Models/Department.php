<?php

namespace App\Models;

use App\Models\Base;
use DB;

class Department extends Base {

    public function getDepartments() {
        return DB::table($this->tblDepartment . " as d")
                        ->leftJoin($this->tblLocation . " as l", "l.id", "=", "d.location_id")
                        ->select("d.*", "l.name as location_name")
                        ->get();
    }

    public function getDepartmentById($id) {
        return DB::table($this->tblDepartment)->where("id", "=", $id)->limit(1)->get();
    }

    public function addDepartment($data) {
        $data["created_at"] = $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblDepartment)->insert($data);
    }

    public function updateDepartmentById($data, $id) {
        $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblDepartment)->where("id", $id)->update($data);
    }

    public function getDepartmentOpts(array $where) {
        $db = DB::table($this->tblDepartment);
        $db->select('id', 'name');
        $db->where($where);
        $db->orderBy('name');
        return $db->get();
    }

}
