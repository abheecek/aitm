<?php

namespace App\Models;

use App\Models\Base;
use DB;

class Manufacture extends Base {

    public function getManufactures() {
        return DB::table($this->tblManfacture)->get();
    }

    public function getManufactureById($id) {
        return DB::table($this->tblManfacture)->where("id", "=", $id)->limit(1)->get();
    }

    public function addManufacture($data) {
        $data["created_at"] = $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblManfacture)->insert($data);
    }

    public function updateManufactureById($data, $id) {
        $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblManfacture)->where("id", $id)->update($data);
    }

}
