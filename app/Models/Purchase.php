<?php

namespace App\Models;

use App\Models\Base;
use DB;

class Purchase extends Base {

    public function getPurchases() {
        return DB::table($this->tblPurchase . " as s")
                        ->leftJoin($this->tblCountry . " as c", "c.country_code", "=", "s.country")
                        ->addSelect("s.*")
                        ->addSelect("c.name as country_name")
                        ->get();
    }

    public function getPurchaseById($id) {
        return DB::table($this->tblPurchase)->where("id", "=", $id)->limit(1)->get();
    }

    public function addPurchase($data) {
        $data["created_at"] = $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblPurchase)->insert($data);
    }

    public function updatePurchaseById($data, $id) {
        $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblPurchase)->where("id", $id)->update($data);
    }

    public function getPurchasesForOpts() {
        return DB::table($this->tblPurchase)
                        ->select(DB::raw('concat(name, " (", city, ")") as location_name'), "id")
                        ->orderBy('location_name', 'asc')
                        ->get();
    }

}
