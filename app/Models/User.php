<?php

namespace App\Models;

use App\Models\Base;
use DB;

class User extends Base {

    public $user_types = array("End User", "Admin", "Global Admin", "Super Admin");

    public function getUsers() {
        return DB::table($this->tblUser . " as u")
                        ->leftJoin($this->tblLocation . " as l", "l.id", "=", "u.location_id")
                        ->leftJoin($this->tblCompany . " as c", "c.id", "=", "u.company_id")
                        ->leftJoin($this->tblDepartment . " as d", "d.id", "=", "u.department_id")
                        ->select("u.*", "l.name as location_name", "c.name as company_name", "d.name as department_name")
                        ->get();
    }

    public function getUserById($id) {
        return DB::table($this->tblUser)->where("id", "=", $id)->limit(1)->get();
    }

    public function getUserProfile($id) {
        return DB::table($this->tblUser . " as u")
                        ->leftJoin($this->tblLocation . " as l", "l.id", "=", "u.location_id")
                        ->leftJoin($this->tblCompany . " as c", "c.id", "=", "u.company_id")
                        ->leftJoin($this->tblDepartment . " as d", "d.id", "=", "u.department_id")
                        ->select("u.*", "l.name as location_name", "c.name as company_name", "d.name as department_name")
                        ->where("u.id", "=", $id)
                        ->get();
    }

    public function addUser($data) {
        if (isset($data["password"])) {
            $data["password"] = bcrypt($data["password"]);
        }
        $data["created_at"] = $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblUser)->insert($data);
    }

    public function updateUserById($data, $id) {
        if (isset($data["password"])) {
            $data["password"] = bcrypt($data["password"]);
        }
        $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblUser)->where("id", $id)->update($data);
    }

    public function getUsersToOpts(array $where) {
        return DB::table($this->tblUser)
                        ->select("id", DB::raw('concat(firstname, " ", lastname, " (", username, ")") as name'))
                        ->where($where)
                        ->orderBy("name")
                        ->get();
    }

}
