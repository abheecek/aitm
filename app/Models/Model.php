<?php

namespace App\Models;

use App\Models\Base;
use DB;

class Model extends Base {

    public function getModels() {
        $db = DB::table($this->tblModel . " as mdl");
        $db->join($this->tblManfacture . " as man", "man.id", "=", "mdl.manufact_id");
        $db->join($this->tblCategory . " as cat", "cat.id", "=", "mdl.category_id");
        $db->select("mdl.id", "mdl.name", "mdl.model_no", "man.name as manufact_name", "cat.name as cat_name", "cat.category_type");
        return $db->get();
    }

    public function getModelById($id) {
        return DB::table($this->tblModel)->where("id", "=", $id)->limit(1)->get();
    }

    public function addModel($data) {
        $data["created_at"] = $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblModel)->insert($data);
    }

    public function updateModelById($data, $id) {
        $data["updated_at"] = date('Y-m-d H:i:s');
        return DB::table($this->tblModel)->where("id", $id)->update($data);
    }

    public function getDeviceModels() {
        $db = DB::table($this->tblModel . " as mdl");
        $db->join($this->tblManfacture . " as man", "man.id", "=", "mdl.manufact_id");
        $db->join($this->tblCategory . " as cat", "cat.id", "=", "mdl.category_id");
        $db->select("mdl.id", DB::raw('concat(man.name, " ", mdl.name, " ", mdl.model_no) as model_name'));
        $db->where("cat.category_type", "=", "Device");
        $db->orderBy("model_name");
        return $db->get();
    }

}
