<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Ticket;
use App\Models\TicketDesc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Mail;

/*
 * Ticket Status :
 * 0 : Open
 * 1 : Processing
 * 2 : Closed
 */
class ITicketController extends Controller{
	protected $ticket;
	protected $ticket_desc;
	private $ticket_status;
	public function __construct(Ticket $ticket, TicketDesc $ticket_desc){
		$this->ticket			=	$ticket;
		$this->ticket_desc		=	$ticket_desc;
		$this->ticket_status	=	array('0' => 'Open', '1' => 'Processing', '3' => 'Closed');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ticketList(Request $request){
    	return view('ticket.i.list');
    }
    
    public function filterData(){
    	$result	=	array();
    	$tickets = $this->ticket->all();
    	foreach($tickets as $k => $v){    		
    		$result['data'][$k][]		=	$v->ticket_title;
    		$result['data'][$k][]		=	$v->ticket_type;
    		$result['data'][$k][]		=	date('d-m-Y', strtotime($v->expected_date));
    		$result['data'][$k][]		=	date('d-m-Y', strtotime($v->created_at));
    		$result['data'][$k][]		=	$this->ticket_status[$v->status];
    		$result['data'][$k][]		=	'<a class="btn" href="'.route('admin.iticket.view', ['id' => $v->id]).'">View</a>';
    	}
    	return $result;
    }
    
    public function view(Request $request, $id){
    	$tickets		=	$this->ticket->where('id', $id)->first();
    	$ticket_desc	=	$this->ticket_desc->where('ticketid', $id)->get();
    	return view('ticket.i.view', ['ticket' => $tickets, 'id' => $id, 'ticket_status'	=>	$this->ticket_status, 'ticket_desc' => $ticket_desc]);
    }
    
    public function fix(Request $request, $id){
    	$validator	=	Validator::make($request->all(), [
    		'description' => 'required'
    	]);
    	if($validator->fails()) {
    		return redirect()->route('admin.iticket.view', ['id' => $id])->withErrors(['message' => 'Validaiton failed, Please send all details properly.']);
    	}
    	$this->ticket->where('id', $id)->update(['status' => $request->input('status')]);
    	
    	$this->ticket_desc->ticketid	=	$id;
    	$this->ticket_desc->desc		=	$request->input('description');
    	$this->ticket_desc->byit		=	1;
    	if($this->ticket_desc->save()){
    		return redirect()->route('admin.iticket.view', ['id' => $id])->withSuccess('Comment added successfully.');
    	}
    	return redirect()->route('admin.iticket.view', ['id' => $id])->withErrors(['message' => 'Error in processing request.']);
    } 
}
