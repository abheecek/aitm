<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Label;

class LabelController extends Controller {

    public function getIndex() {
        return view("labels.index")->with("objLabel", new Label());
    }

    public function addLabel(Request $request) {
        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given Label"
            );
            $data = $request->only("name", "status_type", "notes");
            $objLabel = new Label;
            if ($objLabel->addLabel($data)) {
                $return["msg"] = "Label added successfully";
                $return["status"] = "success";
            }
            $request->session()->flash("msg", $return);
        }
        return view("Labels.add")->with("objLabel", new Label());
    }

    public function editLabel($id, Request $request) {
        $objLabel = new Label;
        $get_Label = $objLabel->getLabelById($id);

        if (count($get_Label) != 1) {
            return redirect()->action("LabelController@getIndex")->with("msg", array(
                        "status" => "warning",
                        "msg" => "No Label found for given data"
            ));
        }

        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to edit given Label"
            );

            $data = $request->only("name", "status_type", "notes");
            if ($objLabel->updateLabelById($data, $id)) {
                $return["msg"] = "Label updated successfully";
                $return["status"] = "success";
                return redirect()->action("LabelController@getIndex")->with("msg", $return);
            }

            $request->session()->flash("msg", $return);
        }

        return view("Labels.edit")->with("objLabel", new Label())
                        ->with("label", $get_Label[0]);
    }

    public function checkDeployable(Request $request, $id) {
        $result = array("result" => false);
        if ($id) {
            $objLabel = new Label();
            $get_Label = $objLabel->getLabelById($id);
            if (count($get_Label) && $get_Label[0]->status_type == "Deployable") {
                $result["result"] = true;
            }
        }
        return response()->json($result);
    }

}
