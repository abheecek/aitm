<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Location;
use App\Models\Company;

class LocationController extends Controller {

    public function getIndex() {
        return view("locations.index")->with("objLocation", new Location());
    }

    public function addLocation(Request $request) {
        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given location"
            );
            $data = $request->only("name", "company_id", "street1", "street2", "city", "state", "country", "zip");
            $objLocation = new Location;
            if ($objLocation->addLocation($data)) {
                $return["msg"] = "Location added successfully";
                $return["status"] = "success";
            }
            $request->session()->flash("msg", $return);
        }
        return view("locations.add")->with("company", new Company)->with("objLocation", new Location());
    }

    public function editLocation($id, Request $request) {
        $objLocation = new Location;
        $get_location = $objLocation->getLocationById($id);

        if (count($get_location) != 1) {
            return redirect()->action("LocationController@getIndex")->with("msg", array(
                        "status" => "warning",
                        "msg" => "No Location found for given data"
            ));
        }

        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to edit given Location"
            );
            $data = $request->only("name", "company_id", "street1", "street2", "city", "state", "country", "zip");
            if ($objLocation->updateLocationById($data, $id)) {
                $return["msg"] = "Location updated successfully";
                $return["status"] = "success";
                return redirect()->action("LocationController@getIndex")->with("msg", $return);
            }
            $request->session()->flash("msg", $return);
        }

        return view("Locations.edit")->with("objLocation", new Location())
                        ->with("location", $get_location[0]);
    }

}
