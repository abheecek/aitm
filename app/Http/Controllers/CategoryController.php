<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller {

    public function getIndex() {
        return view("categories.index")->with("category", new Category);
    }

    public function addCategory(Request $request) {
        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given Category"
            );
            $data = $request->only("name", "category_type");
            $objCategory = new Category;
            if ($objCategory->addCategory($data)) {
                $return["msg"] = "Category added successfully";
                $return["status"] = "success";
            }
            $request->session()->flash("msg", $return);
        }
        return view("categories.add");
    }

    public function editCategory($id, Request $request) {
        $objCategory = new Category;
        $get_comp = $objCategory->getCategoryById($id);

        if (count($get_comp) != 1) {
            return redirect()->action("CategoryController@getIndex")->with("msg", array(
                        "status" => "warning",
                        "msg" => "No Category found for given data"
            ));
        }

        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to update given Category"
            );

            $data = $request->only("name", "category_type");
            if ($objCategory->updateCategoryById($data, $id)) {
                $return["msg"] = "Category updated successfully";
                $return["status"] = "success";
                return redirect()->action("CategoryController@getIndex")->with("msg", $return);
            }

            $request->session()->flash("msg", $return);
        }

        return view("categories.edit")->with("category", $get_comp[0])->with("objCategory", $objCategory);
    }

}
