<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Ticket;
use App\Models\TicketDesc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Mail;

/*
 * Ticket Status : 
 * 0 : Open
 * 1 : Processing
 * 2 : Closed
 */

class UTicketController extends Controller{
	protected $ticket;
	protected $ticket_desc;
	private $ticket_status;
	public function __construct(Ticket $ticket, TicketDesc $ticket_desc){
		$this->ticket	=	$ticket;
		$this->ticket_desc		=	$ticket_desc;
		$this->ticket_status	=	array('0' => 'Open', '1' => 'Processing', '3' => 'Closed');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(){
    	$type = array(
    		'type1'	=> 'Type 1',
    		'type2'	=> 'Type 2',
    		'type3'	=> 'Type 3'
    	);
    	/*Mail::send('admin.ticket.email', array($type), function($message){
    		$message->to('abhishekyadav.vrns@gmail.com','Learning Laravel Support')
    		->subject('Contact using Our Contact Form');
    	});*/
		
        return view('ticket.u.add', compact('type'));
    }
    
    public function addPost(Request $request){
    	$validator	=	Validator::make($request->all(), [
    		'ticket_title' => 'required',
    		'ticket_type'	=>	'required',
    		'description' => 'required',
    		'expected_date' => 'required'
    	]);
    	if($validator->fails()) {
    		return redirect()->route('admin.uticket.add')->withErrors(['message' => 'Validaiton failed, Please send all details properly.']);
    	}
    	
    	$this->ticket->ticket_title	=	$request->input('ticket_title');
    	$this->ticket->ticket_type	=	$request->input('ticket_type');
    	$this->ticket->prob_desc	=	$request->input('description');
    	$this->ticket->expected_date	=	date('Y-m-d', strtotime($request->input('expected_date')));
    	$this->ticket->created_by	=	1;
    	if($this->ticket->save()){
    		return redirect()->route('admin.uticket.add')->withSuccess('Ticket created successfully.');
    	}
    	return redirect()->route('admin.uticket.add')->withErrors(['message' => 'Error in creating ticket.']);
    }
    
    public function ticketList(Request $request){
    	return view('ticket.u.list');
    }
    
    public function filterData(){
    	$result	=	array();
    	$tickets = $this->ticket->all();
    	foreach($tickets as $k => $v){
    		/*$status = "Open";
    		if($v->sol_desc != '')$status = "Resolved";
    		if($v->close == 1)$status = "Closed";*/
    		
    		$result['data'][$k][]		=	$v->ticket_title;
    		$result['data'][$k][]		=	$v->ticket_type;
    		$result['data'][$k][]		=	date('d-m-Y', strtotime($v->expected_date));
    		$result['data'][$k][]		=	date('d-m-Y', strtotime($v->created_at));
    		$result['data'][$k][]		=	$this->ticket_status[$v->status];
    		$result['data'][$k][]		=	'<a class="btn" href="'.route('admin.uticket.view', ['id' => $v->id]).'">View</a>';
    	}
    	return $result;
    }
    
    public function view(Request $request, $id){
    	$tickets	=	$this->ticket->where('id', $id)->first();
    	$ticket_desc	=	$this->ticket_desc->where('ticketid', $id)->get();
    	return view('ticket.u.view', ['ticket' => $tickets, 'id' => $id, 'ticket_status'	=>	$this->ticket_status, 'ticket_desc' => $ticket_desc]);
    }
    
    public function close(Request $request, $id){
    	$validator	=	Validator::make($request->all(), [
    		'description' => 'required'
    	]);
    	if($validator->fails()) {
    		return redirect()->route('admin.uticket.view', ['id' => $id])->withErrors(['message' => 'Validaiton failed, Please send all details properly.']);
    	}
    	$this->ticket->where('id', $id)->update(['status' => $request->input('status')]);
    	 
    	$this->ticket_desc->ticketid	=	$id;
    	$this->ticket_desc->desc		=	$request->input('description');
    	$this->ticket_desc->byit		=	0;
    	if($this->ticket_desc->save()){
    		return redirect()->route('admin.uticket.view', ['id' => $id])->withSuccess('Comment added successfully.');
    	}
    	return redirect()->route('admin.uticket.view', ['id' => $id])->withErrors(['message' => 'Error in processing request.']);
    	
    	/*DB::table('ticket')->where('id', $id)->update(['close' => 1]);
    	return redirect()->route('admin.uticket.view', ['id' => $id])->withSuccess('Ticket closed successfully.');*/
    } 
}
