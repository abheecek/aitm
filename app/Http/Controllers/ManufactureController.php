<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Manufacture;

class ManufactureController extends Controller {

    public function getIndex() {
        return view("manufactures.index")->with("manufacture", new Manufacture);
    }

    public function addManufacture(Request $request) {
        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given Manufacture"
            );
            $data = $request->only("name");
            $objManufacture = new Manufacture;
            if ($objManufacture->addManufacture($data)) {
                $return["msg"] = "Manufacture added successfully";
                $return["status"] = "success";
            }
            $request->session()->flash("msg", $return);
        }
        return view("manufactures.add");
    }

    public function editManufacture($id, Request $request) {
        $objManufacture = new Manufacture;
        $get_manufacture = $objManufacture->getManufactureById($id);

        if (count($get_manufacture) != 1) {
            return redirect()->action("ManufactureController@getIndex")->with("msg", array(
                        "status" => "warning",
                        "msg" => "No Manufacture found for given data"
            ));
        }

        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given Manufacture"
            );

            $data = $request->only("name");
            if ($objManufacture->updateManufactureById($data, $id)) {
                $return["msg"] = "Manufacture updated successfully";
                $return["status"] = "success";
                return redirect()->action("ManufactureController@getIndex")->with("msg", $return);
            }

            $request->session()->flash("msg", $return);
        }

        return view("manufactures.edit")->with("manufacture", $get_manufacture[0]);
    }

}
