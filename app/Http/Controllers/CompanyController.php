<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use DB;

class CompanyController extends Controller {

    public function getIndex() {
        return view("company.index")->with("company", new Company);
    }
    
    public function ajaxCompanies(Request $request) {
        $data = $request->all();
        $objCompany = new Company();
        $db = DB::table($objCompany->tblCompany)->select('id', 'name');
        
        $return = array(
            "draw" => date('is')
        );
        
        $return['recordsTotal'] = $db->count();
        $return['recordsFiltered'] = $return['recordsTotal'];
        $data = $db->get();
        
        $return['data'] = array();
        foreach($data as $d) {
            $return['data'][] = array('a' => $d);
        }
        
        return response()->json($return);
    }

    public function addCompany(Request $request) {
        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given company"
            );
            $data = $request->only("name");
            $objCompany = new Company;
            if ($objCompany->addCompany($data)) {
                $return["msg"] = "Company added successfully";
                $return["status"] = "success";
            }
            $request->session()->flash("msg", $return);
        }
        return view("company.add");
    }

    public function editCompany($id, Request $request) {
        $objCompany = new Company;
        $get_comp = $objCompany->getCompanyById($id);

        if (count($get_comp) != 1) {
            return redirect()->action("CompanyController@getIndex")->with("msg", array(
                        "status" => "warning",
                        "msg" => "No company found for given data"
            ));
        }

        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given company"
            );

            $data = $request->only("name");
            if ($objCompany->updateCompanyById($data, $id)) {
                $return["msg"] = "Company updated successfully";
                $return["status"] = "success";
                return redirect()->action("CompanyController@getIndex")->with("msg", $return);
            }

            $request->session()->flash("msg", $return);
        }

        return view("company.edit")->with("company", $get_comp[0]);
    }

}
