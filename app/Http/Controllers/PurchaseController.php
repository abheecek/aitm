<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Purchase;
use App\Models\Supplier;
use App\Models\Company;
use Auth;
use DB;

class PurchaseController extends Controller {

    public function getIndex() {
        return view("purchases.index")->with("objPurchase", new Purchase());
    }
    
    public function ajaxPurchases(Request $request) {
        $data = $request->all();
        $objPurchase = new Purchase();
        $db = DB::table($objPurchase->tblPurchase . ' as pur');
        $db->join($objPurchase->tblSupplier . ' as sup', 'pur.supplier_id', '=', 'sup.id');
        $db->join($objPurchase->tblCompany . ' as comp', 'pur.company_id', '=', 'comp.id');
        $db->join($objPurchase->tblCountry . ' as coun', 'sup.country', '=', 'coun.country_code');
        $db->select('pur.id', 'pur.invoice_number', 'pur.purchase_date', 'comp.name as company_name', 'sup.name as supplier_name', 'sup.city', 'sup.state', 'coun.name as country');
        
        $return = array(
            "draw" => date('is')
        );
        
        $return['recordsTotal'] = $db->count();
        $return['recordsFiltered'] = $return['recordsTotal'];
        $data = $db->get();
        
        $return['data'] = array();
        foreach($data as $d) {
            $return['data'][] = array('a' => $d);
        }
        
        return response()->json($return);
    }

    public function addPurchase(Request $request) {
        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to add given purchase"
            );
            $data = $request->only("purchase_date", "invoice_number", "company_id", "supplier_id", "notes");
            $data["created_by"] = Auth::user()->id;
            $objPurchase = new Purchase;
            if ($objPurchase->addPurchase($data)) {
                $return["msg"] = "Purchase added successfully";
                $return["status"] = "success";
            }
            $request->session()->flash("msg", $return);
        }
        return view("purchases.add")->with("objPurchase", new Purchase())->with("objSupplier", new Supplier())->with("objCompany", new Company());
    }

    public function editPurchase($id, Request $request) {
        $objPurchase = new Purchase;
        $get_purchase = $objPurchase->getPurchaseById($id);

        if (count($get_purchase) != 1) {
            return redirect()->action("PurchaseController@getIndex")->with("msg", array(
                "status" => "warning",
                "msg" => "No Purchase found for given data"
            ));
        }

        if ($request->isMethod('post')) {
            $return = array(
                "status" => "danger",
                "msg" => "Unable to edit given Purchase"
            );

            $data = $request->only("purchase_date", "invoice_number", "company_id", "supplier_id", "notes");
            if ($objPurchase->updatePurchaseById($data, $id)) {
                $return["msg"] = "Purchase updated successfully";
                $return["status"] = "success";
                return redirect()->action("PurchaseController@getIndex")->with("msg", $return);
            }

            $request->session()->flash("msg", $return);
        }

        return view("Purchases.edit")
            ->with("objPurchase", new Purchase())
            ->with("objSupplier", new Supplier())
            ->with("objCompany", new Company())
            ->with("purchase", $get_purchase[0]);
    }
}
