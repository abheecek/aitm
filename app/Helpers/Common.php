<?php 

namespace App\Helpers;

class Common 
{
	public static function asset($path, $extra=null)
	{
		$add = $extra ? "?" . $extra : "?";
		$ext = env('ASSET_NOCACHE') === true ? $add : env('ASSET_NOCACHE') !== false ? $add . "&ver=" . env('ASSET_NOCACHE') : $add . "&ver=" . date('is');
		return asset($path) . $ext;
	}
}

