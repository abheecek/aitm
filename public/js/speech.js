var baseUrl = window.location.protocol+"//"+document.domain+"/ritm";
var langs =
	[['Afrikaans',       ['af-ZA']],
	 ['Bahasa Indonesia',['id-ID']],
	 ['Bahasa Melayu',   ['ms-MY']],
	 ['CatalÃ ',          ['ca-ES']],
	 ['ÄŒeÅ¡tina',         ['cs-CZ']],
	 ['Deutsch',         ['de-DE']],
	 ['English',         ['en-AU', 'Australia'],
						 ['en-CA', 'Canada'],
						 ['en-IN', 'India'],
						 ['en-NZ', 'New Zealand'],
						 ['en-ZA', 'South Africa'],
						 ['en-GB', 'United Kingdom'],
						 ['en-US', 'United States']],
	 ['EspaÃ±ol',         ['es-AR', 'Argentina'],
						 ['es-BO', 'Bolivia'],
						 ['es-CL', 'Chile'],
						 ['es-CO', 'Colombia'],
						 ['es-CR', 'Costa Rica'],
						 ['es-EC', 'Ecuador'],
						 ['es-SV', 'El Salvador'],
						 ['es-ES', 'EspaÃ±a'],
						 ['es-US', 'Estados Unidos'],
						 ['es-GT', 'Guatemala'],
						 ['es-HN', 'Honduras'],
						 ['es-MX', 'MÃ©xico'],
						 ['es-NI', 'Nicaragua'],
						 ['es-PA', 'PanamÃ¡'],
						 ['es-PY', 'Paraguay'],
						 ['es-PE', 'PerÃº'],
						 ['es-PR', 'Puerto Rico'],
						 ['es-DO', 'RepÃºblica Dominicana'],
						 ['es-UY', 'Uruguay'],
						 ['es-VE', 'Venezuela']],
	 ['Euskara',         ['eu-ES']],
	 ['FranÃ§ais',        ['fr-FR']],
	 ['Galego',          ['gl-ES']],
	 ['Hrvatski',        ['hr_HR']],
	 ['IsiZulu',         ['zu-ZA']],
	 ['Ãslenska',        ['is-IS']],
	 ['Italiano',        ['it-IT', 'Italia'],
						 ['it-CH', 'Svizzera']],
	 ['Magyar',          ['hu-HU']],
	 ['Nederlands',      ['nl-NL']],
	 ['Norsk bokmÃ¥l',    ['nb-NO']],
	 ['Polski',          ['pl-PL']],
	 ['PortuguÃªs',       ['pt-BR', 'Brasil'],
						 ['pt-PT', 'Portugal']],
	 ['RomÃ¢nÄƒ',          ['ro-RO']],
	 ['SlovenÄina',      ['sk-SK']],
	 ['Suomi',           ['fi-FI']],
	 ['Svenska',         ['sv-SE']],
	 ['TÃ¼rkÃ§e',          ['tr-TR']],
	 ['Ð±ÑŠÐ»Ð³Ð°Ñ€ÑÐºÐ¸',       ['bg-BG']],
	 ['PÑƒÑÑÐºÐ¸Ð¹',         ['ru-RU']],
	 ['Ð¡Ñ€Ð¿ÑÐºÐ¸',          ['sr-RS']],
	 ['í•œêµ­ì–´',            ['ko-KR']],
	 ['ä¸­æ–‡',             ['cmn-Hans-CN', 'æ™®é€šè¯ (ä¸­å›½å¤§é™†)'],
						 ['cmn-Hans-HK', 'æ™®é€šè¯ (é¦™æ¸¯)'],
						 ['cmn-Hant-TW', 'ä¸­æ–‡ (å°ç£)'],
						 ['yue-Hant-HK', 'ç²µèªž (é¦™æ¸¯)']],
	 ['æ—¥æœ¬èªž',           ['ja-JP']],
	 ['Lingua latÄ«na',   ['la']]];

var recognition = '';
var final_transcript = '';
var recognizing = false;
var ignore_onend;
var start_timestamp;
var prev_ref;
if (!('webkitSpeechRecognition' in window)) {
  	//speech_wrap.className += ' hidden';
  	document.getElementsByClassName('fa-mic-wr').className += ' hidden';
	var x = document.getElementsByClassName('fa-mic-wr');
	var i;
	for (i = 0; i < x.length; i++) {
		//x[i].className += ' hidden';
	}
}else {
	init_speech();
}

function activateSpeech(event) {
  	if (recognizing) {
   		recognition.stop();
    	return;
  	}
	
  	final_transcript = '';
  	recognition.lang = select_dialect.value;
  	recognition.start();
  	ignore_onend = false;
  	start_img.src = baseUrl+'/public/imgs/speech/mic-slash.gif';
  	showInfo('info_allow');
  	start_timestamp = event.timeStamp;
	document.getElementsByClassName("speech-fill")[0].focus();
}

function showInfo(s) {
	console.log(s);return false;
  if (s) {
    for (var child = info.firstChild; child; child = child.nextSibling) {
      if (child.style) {
        child.style.display = child.id == s ? 'inline' : 'none';
      }
    }
    info.style.visibility = 'visible';
  } else {
    info.style.visibility = 'hidden';
  }
}

function init_speech(){
	for (var i = 0; i < langs.length; i++) {
		select_language.options[i] = new Option(langs[i][0], i);
	}
	select_language.selectedIndex = 6;
	updateCountry();
	select_dialect.selectedIndex = 6;
	
	start_button.style.display = 'inline-block';
	recognition = new webkitSpeechRecognition();
	recognition.continuous = true;
	recognition.interimResults = true;
	
	recognition.onstart = function() {
		recognizing = true;
		showInfo('info_speak_now');
		start_img.src = baseUrl+'/public/imgs/speech/mic-animate.gif';
	};
	
	recognition.onerror = function(event) {
		if (event.error == 'no-speech') {
			start_img.src = baseUrl+'/public/imgs/speech/mic.gif';
		  	showInfo('info_no_speech');
		  	ignore_onend = true;
		}
		if (event.error == 'audio-capture') {
		  	start_img.src = baseUrl+'/public/imgs/speech/mic.gif';
		  	showInfo('info_no_microphone');
		  	ignore_onend = true;
		}
		if (event.error == 'not-allowed') {
		  	if (event.timeStamp - start_timestamp < 100) {
				showInfo('info_blocked');
		  	} else {
				showInfo('info_denied');
		 	}
		  ignore_onend = true;
		}
	};
	
	recognition.onend = function() {
		recognizing = false;
		if (ignore_onend) {
			return;
		}
		start_img.src = baseUrl+'/public/imgs/speech/mic.gif';
		if (!final_transcript) {
		  showInfo('info_start');
		  return;
		}
		showInfo('');
	};
	
	recognition.onresult = function(event) {
		var cur_element		=	document.activeElement;
		if(!hasClass(cur_element, 'speech-fill')){
			console.log('Please select appropriate field');	
			return false;
		}
		//if(cur_element.name != prev_ref){
			final_transcript = cur_element.value;
		//}
		var interim_transcript = '';
		for (var i = event.resultIndex; i < event.results.length; ++i) {
			if (event.results[i].isFinal) {
				final_transcript += event.results[i][0].transcript;
		  	}
		}
		console.log(final_transcript);
		final_transcript = capitalize(final_transcript);
		cur_element.value = linebreak(final_transcript);
		prev_ref	=	cur_element.name;
		//interim_span.innerHTML = linebreak(interim_transcript);
		/*if (final_transcript || interim_transcript) {
		  showButtons('inline-block');
		}*/
	};
}

function updateCountry() {
	for (var i = select_dialect.options.length - 1; i >= 0; i--) {
		select_dialect.remove(i);
  	}
  	var list = langs[select_language.selectedIndex];
  	for (var i = 1; i < list.length; i++) {
		select_dialect.options.add(new Option(list[i][1], list[i][0]));
  	}
  	select_dialect.style.visibility = list[1].length == 1 ? 'hidden' : 'visible';
}

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

var first_char = /\S/;
function capitalize(s) {
  return s.replace(first_char, function(m) { return m.toUpperCase(); });
}

var two_line = /\n\n/g;
var one_line = /\n/g;
function linebreak(s) {
  return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}