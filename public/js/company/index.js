$(document).ready(function() {
    
    $.fn.dataTable.render.actions = function( url ) {
        return function ( d ) {
            var str = [];
            str.push("<a href='" + url.edit + "/" + d + "' data-id='" + d + "' >Edit</a>");
            return str.join(' | ');
        };
    };
    
    var table = $('#mytable');
    table.DataTable({
        autoWidth: false,
        aoColumnDefs: [{
            'bSortable': false,
            'aTargets': [0]
        }, {
            targets: 0,
            render: $.fn.dataTable.render.actions(window.config.url)
        }],
        order: [[1, 'desc']],
        processing: true,
        serverSide: true,
        ajax: { 
            url: window.config.url.companies, 
            type: "post",
            data: function(d) {
                d._token = window.config.token;
            }
        },
        columns: [
            { data: 'a.id'},
            {data: 'a.name'}
        ]
    });
});