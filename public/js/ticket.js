$(document).ready(function(){
	$('.datepicker').datepicker({
        format	: 	'dd-mm-yyyy',
        startDate	: 	'+0d',
        autoclose: true
    });
	
	$('.showCalender').click(function(){
		$(".datepicker").datepicker("show");
	});
});